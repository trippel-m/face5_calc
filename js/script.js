/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50*/
/*global define */
/*jslint browser: true*/
/*global $, jQuery, alert*/
(function () {
    "use strict";
    var tile_size_horisontal,
        tile_size_vertical = parseFloat($('#panelHeigth').val()) / 1000,
        tile_pitch = parseFloat($('#panelPitch').val()),
        tile_resolution_horisontal = Math.floor(parseFloat($('#panelWidth').val()) / tile_pitch),
        tile_resolution_vertical = Math.floor(parseFloat($('#panelHeigth').val()) / tile_pitch),
        tile_weigth = parseFloat($('#panelWeigth').val()),
        tile_power_consumption = parseFloat($('#panelPower').val()),
        rigging_single_headers_weigth = parseFloat($('#singleHeaderWeigth').val()),
        rigging_double_headers_weigth = parseFloat($('#doubleHeaderWeigth').val()),
        flight_weigth = parseFloat($('#flightCaseWeigth').val()),
        tiles_pr_flight = parseFloat($('#fligthCaseCapacity').val()),
        accessory_flights = parseFloat($('#accessoryFlights').val()),
        output_capacity = parseFloat($('#outputCapacity').val()),
        controller_outputs = parseFloat($('#controllerOutputs').val()),
        controller_pixel_capacity = output_capacity * controller_outputs,
        controller_tiles_per_port = output_capacity / (tile_resolution_horisontal * tile_resolution_vertical),
        tile_cable_weigth = 0.4,
        rigging_steel_cable_weigth = 2.3,
        rigging_single_headers = 0,
        rigging_double_headers = 0,
        horisontal_pieces = 0,
        vertical_pieces = 0;
        
    
    function set_variables() {
            tile_size_horisontal = parseFloat($('#panelWidth').val()) / 1000,
            tile_size_vertical = parseFloat($('#panelHeigth').val()) / 1000,
            tile_pitch = parseFloat($('#panelPitch').val()),
            tile_resolution_horisontal = Math.floor(parseFloat($('#panelWidth').val()) / tile_pitch),
            tile_resolution_vertical = Math.floor(parseFloat($('#panelHeigth').val()) / tile_pitch),
            tile_weigth = parseFloat($('#panelWeigth').val()),
            tile_power_consumption = parseFloat($('#panelPower').val()),
            rigging_single_headers_weigth = parseFloat($('#singleHeaderWeigth').val()),
            rigging_double_headers_weigth = parseFloat($('#doubleHeaderWeigth').val()),
            flight_weigth = parseFloat($('#flightCaseWeigth').val()),
            tiles_pr_flight = parseFloat($('#fligthCaseCapacity').val()),
            accessory_flights = parseFloat($('#accessoryFlights').val()),
            output_capacity = parseFloat($('#outputCapacity').val()),
            controller_outputs = parseFloat($('#controllerOutputs').val()),
            controller_pixel_capacity = output_capacity * controller_outputs,
            controller_tiles_per_port = output_capacity / (tile_resolution_horisontal * tile_resolution_vertical);
    }
    set_variables();

    function calculate_screen() {
        var tile_amount = horisontal_pieces * vertical_pieces,
            rigging_steel_shackles,
            horisontal_length = horisontal_pieces * tile_size_horisontal,
            vertical_length = vertical_pieces * tile_size_vertical,
            screen_weigth,
            transport_weigth,
            flight_amount,
            power_consumption = tile_amount * tile_power_consumption,
            resolution_horisontal = horisontal_pieces * tile_resolution_horisontal,
            resolution_vertical = vertical_pieces * tile_resolution_vertical,
            total_pixels = resolution_horisontal * resolution_vertical,
            controllers_needed = Math.ceil(total_pixels / controller_pixel_capacity),
            controller_outputs_needed = Math.ceil(tile_amount / controller_tiles_per_port),
            aspect_ratio;
 
        // Check if errors or warnings needs to be printed
        if (horisontal_pieces < 1 || vertical_pieces < 1) {
            $('#error').html("<li class='alert alert-danger'>Screen must be atleast 1x1</li>");
            return;
        } else if (vertical_pieces > 14) {
            $('#error').html("<li class='alert alert-danger'>Screen has max height of 14 tiles</li>");
        } else if (horisontal_pieces === 1 && vertical_pieces > 5) {
            $('#error').html("<li class='alert alert-warning'>All tiles above the bottom 5 tiles needs to be connected with a connection plate in a single column configuration</li>");
        } else if (horisontal_pieces > 1 && vertical_pieces > 10) {
            $('#error').html("<li class='alert alert-warning'>All tiles above the bottom 10 tiles needs to be connected with a connection plate in a multi column configuration</li>");
        } else {
            $('#error').html("");
        }


        //Functions that controll if a number is odd or even
        function isEven(n) {
            return n % 2 === 0;
        }

        function isOdd(n) {
            return Math.abs(n % 2) === 1;
        }

        //Calculate Aspect Ratio
        function aspectRatio() {

            
            
            
            function gcd(a, b) {
                return (b === 0) ? a : gcd(b, a % b);
            }
            
            if (isNaN(horisontal_pieces) || isNaN(vertical_pieces)) {
                return;
            } else if (horisontal_pieces < 1 || vertical_pieces < 1) {
                return;
            } else {
                var r = gcd(resolution_horisontal, resolution_vertical);
                aspect_ratio = resolution_horisontal / r + ":" + resolution_vertical / r + " / " + (resolution_horisontal / resolution_vertical).toFixed(3);
            }
        }
        aspectRatio();

        // Calculate headers needed to hang screen
        if (horisontal_pieces === 1) {
            rigging_single_headers = 1;
            rigging_double_headers = 0;
            rigging_steel_shackles = 3;
        } else if (horisontal_pieces === 2) {
            rigging_single_headers = 0;
            rigging_double_headers = 1;
            rigging_steel_shackles = 3;
        } else if (isEven(horisontal_pieces)) {
            rigging_double_headers = horisontal_pieces / 2;
            rigging_single_headers = 0;

            if (rigging_double_headers === 2) {
                rigging_steel_shackles = 4;
            } else {
                rigging_steel_shackles = (rigging_double_headers - 2) + 4;
            }
        } else if (isOdd(horisontal_pieces)) {
            rigging_double_headers = (horisontal_pieces - 1) / 2;
            rigging_single_headers = 1;

            if (rigging_double_headers === 1) {
                rigging_steel_shackles = 4;
            } else {
                rigging_steel_shackles = (rigging_double_headers * 2) + 1;
            }
        }

        screen_weigth = ((horisontal_pieces * vertical_pieces) * (tile_weigth + tile_cable_weigth)) + (rigging_double_headers * rigging_double_headers_weigth) + (rigging_single_headers * rigging_single_headers_weigth) + (rigging_steel_shackles * rigging_steel_cable_weigth);

        // Round and print screen specs
        $('#screenHeight').text(vertical_length.toFixed(2) + " m");
        $('#screenWidth').text(horisontal_length.toFixed(2) + " m");
        $('#tileAmount').text(tile_amount);
        $('#screenWeigth').text(screen_weigth.toFixed(2) + " Kg");
        $('#resolutionHorisontal').text(resolution_horisontal + " px");
        $('#resolutionVertical').text(resolution_vertical + " px");
        $('#totalPixels').text(total_pixels + " px");
        $('#controllersNeeded').text(controllers_needed);
        $('#p3outputsNeeded').text(controller_outputs_needed);
        $('#singleHeaders').text(rigging_single_headers);
        $('#doubleHeaders').text(rigging_double_headers);
        $('#rigging').text(rigging_steel_shackles);
        $('#aspectRatio').text(aspect_ratio);
        $('#powerConsumption').text(power_consumption + " W");
        $('#needed16A').text(Math.ceil((power_consumption / 230) / 16) + " x 16A 230v");
        $('#ampere400').text(Math.ceil(power_consumption / (Math.sqrt(3) * 400)));
        $('#ampere230').text(Math.ceil(power_consumption / (Math.sqrt(3) * 230)));
        $('#flightsNumber').text(Math.ceil(tile_amount / tiles_pr_flight) + accessory_flights);
        $('#transportWeigth').text((screen_weigth + (Math.ceil(tile_amount / tiles_pr_flight) * flight_weigth) + 100).toFixed(2) + " Kg");


        function draw_screen() {
            var i,
                double_headers_needed;

            $('#drawing').css('width', horisontal_pieces * 100 + 'px');
            
            /* Scaling av tegning til sidestørrelse */
            if ($(window).width() < horisontal_pieces * 100 + 50) {
                $('#drawing').css('transform', 'scale(' + $( window ).width() / (horisontal_pieces * 110) + ')');
                console.log($( window ).width() / (horisontal_pieces * 150))
            } else {
                $('#container').css('width', $(window).width() - 20 + 'px');
            }
            
            $('#container').css('padding-bottom', vertical_pieces * 100 + 'px');

            function draw_vertical() {
                $('#tiles').append('<style>.tile:nth-child(' + horisontal_pieces + 'n+2){clear: left;}</style>');
                for (i = 0; i < vertical_pieces * horisontal_pieces; i++) {
                    $('#tiles').append('<div class="tile"></div>');
                }
            }

            if (horisontal_pieces === 1) {
                $('#headerRow').html('<div class="singleheader"></div>');

                $('#tiles').append('<style>.tile{clear: both;}</style>');
                draw_vertical();

            } else if (horisontal_pieces === 2) {
                $('#headerRow').html('<div class="doubleheader"></div>');

                draw_vertical();

            } else if (horisontal_pieces === 3) {
                $('#headerRow').html('<div class="doubleheader edge"></div><div class="singleheader right"></div>');

                draw_vertical();

            } else if (isEven(horisontal_pieces)) {
                $('#headerRow').html('<div class="doubleheader edge">');

                for (i = 0; i < horisontal_pieces / 2 - 2; i++) {
                    $('#headerRow').append('<div class="doubleheader middle">');
                }

                $('#headerRow').append('<div class="doubleheader edge">');

                draw_vertical();

            } else if (isOdd(horisontal_pieces)) {
                double_headers_needed = (horisontal_pieces - 5) / 2;

                $('#headerRow').html('<div class="doubleheader edge">');


                for (i = 0; i < Math.ceil(double_headers_needed / 2); i++) {
                    $('#headerRow').append('<div class="doubleheader middle">');
                }
                $('#headerRow').append('<div class="singleheader middle">');
                for (i = 0; i < Math.floor(double_headers_needed / 2); i++) {
                    $('#headerRow').append('<div class="doubleheader middle">');
                }


                $('#headerRow').append('<div class="doubleheader edge">');

                draw_vertical();

            }
        }
        $('#headerRow').html('');
        $('#tiles').html('');
        draw_screen();
    }


    // Calculate tile numbers based on prefered width/height
    function prefered_calc() {
        
        
        var prefered_width = parseFloat($('#preferedWidth').val(), 10),
            prefered_height = parseFloat($('#preferedHeight').val(), 10);
        if (isNaN(prefered_width)) {
            prefered_width = 0;
        }
        if (isNaN(prefered_height)) {
            prefered_height = 0;
        }
        horisontal_pieces = Math.round((prefered_width / 100) / tile_size_horisontal);
        vertical_pieces = Math.round((prefered_height / 100) / tile_size_vertical);
        $('#horisontal').val(horisontal_pieces);
        $('#vertical').val(vertical_pieces);
        calculate_screen();
    }

    function led_calc() {
        
        set_variables();
        horisontal_pieces = parseFloat($('#horisontal').val(), 10);
        vertical_pieces = parseFloat($('#vertical').val(), 10);
        if (isNaN(horisontal_pieces)) {
            horisontal_pieces = 0;
        }
        if (isNaN(vertical_pieces)) {
            vertical_pieces = 0;
        }
        $('#preferedWidth').val(Math.floor(horisontal_pieces * tile_size_horisontal * 100));
        $('#preferedHeight').val(Math.floor(vertical_pieces * tile_size_vertical * 100));
        calculate_screen();
        
    }


    $('#horisontal').keyup(led_calc);
    $('#vertical').keyup(led_calc);
    $('#preferedWidth').keyup(prefered_calc);
    $('#preferedHeight').keyup(prefered_calc);

    $('#settings').on('keyup', 'input', set_variables);
    
})();
